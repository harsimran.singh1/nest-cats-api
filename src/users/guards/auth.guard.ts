import { Injectable, CanActivate, ExecutionContext, UnauthorizedException } from '@nestjs/common';
import { Observable } from 'rxjs';
import * as jwt from 'jsonwebtoken';
import { UsersService } from '../users.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private userService: UsersService) {}
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();

    try{
      const token = request.headers.jwt;
      const decoded = jwt.verify(token, 'secret');
      const userId = decoded.id;
      const user = this.userService.find(userId);
      if (user) request.authUser = user;
  
      return true;
    }catch(e){
      throw new UnauthorizedException()
    }
  }
}
