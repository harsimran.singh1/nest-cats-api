import {
  Controller,
  Post,
  UsePipes,
  Body,
  Get,
  Param,
  Put,
  Delete,
  Query,
  UseGuards,
} from '@nestjs/common';
import { CatsService } from './cats.service';
import { CreateCatsDto } from './dtos/create-cats.dto';
import { JoiValidationPipe } from './pipes/joi-validation.pipe';
import { CatsSchema } from './schemas/cats.schema';
import { updateCatDto } from './dtos/update-cats.dto';
import { AuthGuard } from 'src/users/guards/auth.guard';

@Controller('cats')
// @UseGuards(AuthGuard)
export class CatsController {
  constructor(private catsService: CatsService) {}

  @Get('/search')
  findByFilter(
    @Query('age_lte') age_lte: string,
    @Query('age_gte') age_gte: string,
  ) {
    return this.catsService.findByFilter(parseInt(age_lte), parseInt(age_gte));
  }

  @Post('/')
  @UsePipes(new JoiValidationPipe(CatsSchema))
  async create(@Body() body: CreateCatsDto) {
    const { name, age, breed } = body;
    return this.catsService.create(name, age, breed);
  }

  @Put('/:id')
  update(@Param('id') id: string, @Body() body: updateCatDto) {
    return this.catsService.update(parseInt(id), body);
  }

  @Delete('/:id')
  remove(@Param('id') id: string) {
    return this.catsService.remove(parseInt(id));
  }

  @Get('/:id')
  findOne(@Param('id') id: string) {
    return this.catsService.findOne(parseInt(id));
  }

  @Get()
  findList() {
    return this.catsService.find();
  }
}
