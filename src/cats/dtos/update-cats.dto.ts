import { IsNumber, IsOptional, IsString } from 'class-validator';

export class updateCatDto {
  @IsString()
  @IsOptional()
  name: string;

  @IsNumber()
  @IsOptional()
  age: number;

  @IsString()
  @IsOptional()
  breed: string;
}
