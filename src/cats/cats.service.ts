import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { object } from 'joi';
import { Repository } from 'typeorm';
import { CreateCatsDto } from './dtos/create-cats.dto';
import { Cat } from './entities/cats.entity';

@Injectable()
export class CatsService {
  constructor(@InjectRepository(Cat) private repo: Repository<Cat>) {}

  async create(name: string, age: number, breed: string) {
    const cat = await this.repo.create({ name, age, breed });
    return this.repo.save(cat);
  }

  find() {
    return this.repo.find();
  }

  findOne(id: number) {
    return this.repo.findOne(id);
  }

  async findByFilter(
    age_lte: number,
    age_gte: number,
  ): Promise<CreateCatsDto[]> {
    const cats = await this.repo.find({});
    let catArr = [];
    cats.map((cat) => {
      if (cat.age > age_lte && cat.age < age_gte) catArr.push(cat);
    });
    return catArr;
  }

  async update(id: number, attrs: Partial<Cat>) {
    const cat = await this.findOne(id);

    if (!cat) {
      throw new NotFoundException('not found!!!');
    }

    Object.assign(cat, attrs);
    return this.repo.save(cat);
  }

  async remove(id: number) {
    const cat = await this.findOne(id);

    if (!cat) {
      throw new NotFoundException('not found!!!');
    }

    return this.repo.remove(cat);
  }
}
