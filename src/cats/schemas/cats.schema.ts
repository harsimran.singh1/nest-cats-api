import * as Joi from 'joi';

export const CatsSchema = Joi.object({
  id: Joi.number(),
  name: Joi.string().max(20).required(),
  age: Joi.number().min(1).max(30).required(),
  breed: Joi.string().max(20).required(),
}).options({ abortEarly: false, allowUnknown: true });
