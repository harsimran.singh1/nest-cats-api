import { IsNumber, IsString } from "class-validator";

export class CreateCatsDto {
  @IsString()
  name: string;

  @IsNumber()
  age: number;

  @IsString()
  breed: string;
}
