import { BadRequestException, HttpException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class UsersService {
  constructor(@InjectRepository(User) private repo: Repository<User>) {}

  async signup(email: string, password: string) {
    const users = await this.repo.find({ email });
    if (users.length) {
      throw new BadRequestException(
        'email is already in use!!! please use different email to create account....',
      );
    }

    const hashedPassword = await bcrypt.hash(password, 12);

    const user = this.repo.create({
      email,
      password: hashedPassword,
    });

    return this.repo.save(user);
  }

  async signin(email: string, password: string) {
    const user = await this.repo.find({ email });

    if (user && (await bcrypt.compare(password, user[0].password))) {
      const token = await jwt.sign(
        { id: user[0].id, time: Date.now() },
        'secret',
        { expiresIn: '1h' },
      );

      return { email, password, jwt: token };
    } else throw new HttpException('Invalid credentials', 404);
  }

  getUser() {
    return this.repo.find();
  }

  find(id:number){
      return this.repo.find({id});
  }
}
