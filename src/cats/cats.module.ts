import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from 'src/users/users.module';
import { UsersService } from 'src/users/users.service';
import { CatsController } from './cats.controller';
import { CatsService } from './cats.service';
import { Cat } from './entities/cats.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Cat]), UsersModule],
  controllers: [CatsController],
  providers: [CatsService],
})
export class CatsModule {}
