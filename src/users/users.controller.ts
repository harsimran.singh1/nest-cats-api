import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { CreateUserDto } from './dtos/create-user.dto';
import { AuthGuard } from './guards/auth.guard';
import { UsersService } from './users.service';

@Controller('auth')
export class UsersController {
  constructor(private userService: UsersService) {}

  @Post('/signup')
  signup(@Body() body: CreateUserDto) {
    return this.userService.signup(body.email, body.password);
  }

  @Post('/signin')
  signin(@Body() body: CreateUserDto) {
    return this.userService.signin(body.email, body.password);
  }

  @UseGuards(AuthGuard)
  @Get()
  getUser() {
    return this.userService.getUser();
  }
}
